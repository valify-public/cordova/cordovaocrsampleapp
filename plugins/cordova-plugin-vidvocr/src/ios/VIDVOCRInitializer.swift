import UIKit
import VIDVOCR

@objc class VIDVOCRInitializer: NSObject, VIDVOCRDelegate {
    fileprivate var returnDataToJS:Observable<Bool> = Observable(false)
    var jsonResponse = ""
    var success = false
    var capturedImages = [CapturedImageData]()
    var dict = NSMutableDictionary()
    var fromCapuredImages = false
    func onOCRResult(result: VIDVOCRResponse) {
        switch result {
        case .success(let data):
             dict = VIDVResultToDict(VIDVResult: data, hasResult: true, errorMessage: nil, errorCode: nil, step: nil, state: "finalResult")
            success = true
        case .builderError(let code, let message):
            dict = VIDVResultToDict(VIDVResult: nil, hasResult: false, errorMessage: message, errorCode: code, step: nil, state: nil)
            success = false
        case .serviceFailure(let code, let message, let data):
             dict = VIDVResultToDict(VIDVResult: data, hasResult: true, errorMessage: message, errorCode: code, step: nil, state: nil)
            success = false
        case .userExit(let step, let data):
             dict = VIDVResultToDict(VIDVResult: data, hasResult: true, errorMessage: nil, errorCode: 0, step: step, state: nil)
            success = false
            
        case .capturedImages(capturedImageData: let capturedImageData):
            do {
                let nameValuePairs = NSMutableDictionary()
                let jsonEncoder = JSONEncoder()
                let capturedImagesData = try jsonEncoder.encode(capturedImageData)
//                let capturedImagesResult = String(data: capturedImagesData, encoding: .utf8)
                let capturedImagesJson = try JSONSerialization.jsonObject(with: capturedImagesData, options: []) as? [String : Any]
                dict["nameValuePairs"] = nameValuePairs
                nameValuePairs["state"] = "onGoingData"
                nameValuePairs["capturedImage"] = capturedImagesJson
                success = true
                fromCapuredImages = true
            } catch {
                return
            }
            
        }
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            let json = String(data: jsonData, encoding: .utf8)
            self.jsonResponse = json ?? ""
            returnDataToJS.value = true
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
    }
    
    
    func VIDVResultToDict(VIDVResult: VIDVOCRResult?, hasResult: Bool, errorMessage: String?, errorCode: Int?, step: String?, state: String?) -> NSMutableDictionary {
        
        let dict = NSMutableDictionary()
        let nameValuePairs = NSMutableDictionary()
        let valifyData = NSMutableDictionary()
        
        dict["nameValuePairs"] = nameValuePairs
        if let state = state {
            nameValuePairs["state"] = state
        }
        
        if errorCode != nil {
            nameValuePairs["errorCode"] = errorCode
        }
        if errorMessage != nil {
            nameValuePairs["errorMessage"] = errorMessage
        }
        if step != nil {
            nameValuePairs["step"] = step
        }
        
        if hasResult {
            do {
                let jsonEncoder = JSONEncoder()
                let jsonData = try jsonEncoder.encode(VIDVResult)
//                let jsonVIDVResult = String(data: jsonData, encoding: .utf8)
                let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String : Any]
                nameValuePairs["ocrResult"] = json
            } catch {
                print("Failed to encode VIDV Result: \(error.localizedDescription)")
            }
        }
        
        return dict
    }
    
    // function to start valify
    @objc func startOCR(sender: UIViewController, argArr: [NSDictionary], completion: @escaping (Bool, String) -> ()) {
        
        let arg = argArr.first!
        let baseURL = arg.value(forKey: "base_url") as? String ?? ""
        let accessToken = arg.value(forKey: "access_token") as? String ?? ""
        let bundle = arg.value(forKey: "bundle_key") as? String ?? ""
        let languageString = arg.value(forKey: "language") as? String ?? ""
        let documentVerification  = arg.value(forKey: "document_verification") as? Bool ?? false
        let dataValidation  = arg.value(forKey: "data_validation") as? Bool ?? false
        let returnDataValidationError  = arg.value(forKey: "return_data_validation_error") as? Bool ?? false
        let reviewData  = arg.value(forKey: "review_data") as? Bool ?? false
        let captureOnlyMode  = arg.value(forKey: "capture_only_mode") as? Bool ?? true
        let headers  = arg.value(forKey: "headers") as? [String:String] ?? [:]
        let primaryColor = arg.value(forKey: "primary_color") as? String ?? ""

         DispatchQueue.main.async {[weak self] in
            guard let self = self else {return}
            var builder = OCRBuilder()
            builder = builder.setDataValidation(validate: dataValidation)
                .setDocumentVerification(verify: documentVerification)
                .setlLanguage(language: languageString)
                .setBundleKey(bundle)
                .setBaseUrl(baseURL)
                .setAccessToken(accessToken)
                .setReviewData(review: reviewData)
                .setCaptureOnlyMode(captureOnlyMode)
                .setReturnValidationError(returnDataValidationError)
            builder = builder.setPrimaryColor(color: self.hexStringToUIColor(hex: primaryColor))
            builder = builder.setHeaders(headers: headers)
            builder.start(vc: sender, ocrDelegate: self)
        }

        returnDataToJS.observe { send in
            if send {
                completion(self.success, self.jsonResponse)
            }
        }
    }

    func hexStringToUIColor(hex: String?) -> UIColor {
        guard let hex = hex  else {return hexStringToUIColor(hex: "#62CBC9")}
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        
        if (cString.count) != 6 {
            return UIColor.gray
        }
        
        var rgbValue: UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}


fileprivate class Observable<T> {
    
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ v: T) {
        value = v
    }
    
    func bind(_ listener: Listener?) {
        self.listener = listener
    }
    
    func observe(_ listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
}
