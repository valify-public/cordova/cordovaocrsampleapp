/********* VIDVOCR.m Cordova Plugin Implementation *******/

// #import <Cordova/CDV.h>

// @interface VIDVOCR : CDVPlugin {
//   // Member variables go here.
// }

// - (void)coolMethod:(CDVInvokedUrlCommand*)command;
// @end

// @implementation VIDVOCR

// - (void)coolMethod:(CDVInvokedUrlCommand*)command
// {
//     CDVPluginResult* pluginResult = nil;
//     NSString* echo = [command.arguments objectAtIndex:0];

//     if (echo != nil && [echo length] > 0) {
//         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
//     } else {
//         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
//     }

//     [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
// }

// @end



#import <Cordova/CDV.h>
#import "HelloWorld2-Swift.h" // change it with #import "ProductModuleName-Swift.h"


@interface VIDVOCRPlugin : CDVPlugin {
    // Member variables go here.
}


- (void)startOCR:(CDVInvokedUrlCommand*)command;
@end


@implementation VIDVOCRPlugin

// calling the swift code from obejctive c
- (void)startOCR:(CDVInvokedUrlCommand*)command
{
    __block CDVPluginResult* pluginResult = nil;
    NSArray *arr = command.arguments;
    VIDVOCRInitializer *instance = [VIDVOCRInitializer new];
    [instance startOCRWithSender:self.viewController argArr:arr completion:^(BOOL success, NSString * _Nonnull response) {
        if (success) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:response];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:response];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            
        }
    }];
    
}
@end
