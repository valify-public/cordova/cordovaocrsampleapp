package cordova.plugin.vidvocr;

import android.app.Activity;

import com.google.gson.Gson;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import me.vidv.vidvocrsdk.sdk.BuilderError;
import me.vidv.vidvocrsdk.sdk.CapturedImages;
import me.vidv.vidvocrsdk.sdk.ServiceFailure;
import me.vidv.vidvocrsdk.sdk.Success;
import me.vidv.vidvocrsdk.sdk.UserExit;
import me.vidv.vidvocrsdk.sdk.VIDVOCRConfig;
import me.vidv.vidvocrsdk.sdk.VIDVOCRListener;
import me.vidv.vidvocrsdk.sdk.VIDVOCRResponse;

public class VIDVOCRActivity extends Activity {

    private CallbackContext callbackContext;

    @Override
    protected void onStart() {
        super.onStart();
        callbackContext = cordova_plugin_vidvocr.VIDVOCRPlugin.callbackContext;
        startService();
    }

    private void startService() {
        VIDVOCRConfig.Builder builder = new VIDVOCRConfig.Builder();
        builder.setBaseUrl(getIntent().getExtras().getString("base_url"))
                .setAccessToken(getIntent().getExtras().getString("access_token"))
                .setBundleKey(getIntent().getExtras().getString("bundle_key"))
                .setLanguage(getIntent().getExtras().getString("language"));

        if (getIntent().hasExtra("document_verification")) {
            builder.setDocumentVerification(getIntent().getExtras().getBoolean("document_verification"));
        }
        if (getIntent().hasExtra("data_validation")) {
            builder.setDataValidation(getIntent().getExtras().getBoolean("data_validation"));
        }
        if (getIntent().hasExtra("return_data_validation_error")) {
            builder.setReturnValidationError(getIntent().getExtras().getBoolean("return_data_validation_error"));
        }
        if (getIntent().hasExtra("review_data")) {
            builder.setReviewData(getIntent().getExtras().getBoolean("review_data"));
        }
        if (getIntent().hasExtra("capture_only_mode")) {
            builder.setCaptureOnlyMode(getIntent().getExtras().getBoolean("capture_only_mode"));
        }
        if (getIntent().hasExtra("headers")) {
            builder.setHeaders((HashMap<String, String>) getIntent().getExtras().get("headers"));
        }
        // if (getIntent().hasExtra("ssl_certificate")) {
        //     //    builder.setSSLCertificate(getIntent().getExtras().get("ssl_certificate"));
        // }
           if (getIntent().hasExtra("primary_color") && !getIntent().getExtras().getString("primary_color").equals("")) {
                builder.setPrimaryColor(Color.parseColor(getIntent().getExtras().getString("primary_color")));
            }

        builder.start(cordova.plugin.vidvocr.VIDVOCRActivity.this, new VIDVOCRListener() {
            @Override
            public void onOCRResult(VIDVOCRResponse response) {
                if (response instanceof Success) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                         jsonObject.put("state", "finalResult");
                        jsonObject.put("ocrResult", ((Success) response).getData());
                       
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String jsonInString = new Gson().toJson(jsonObject);

                    PluginResult resultado = new PluginResult(PluginResult.Status.OK, jsonInString);

                    resultado.setKeepCallback(true);
                    callbackContext.sendPluginResult(resultado);

                    finish();// Exit of this activity !
                } else if (response instanceof BuilderError) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("errorCode", ((BuilderError) response).getCode());
                        jsonObject.put("errorMessage", ((BuilderError) response).getMessage());
                  
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String jsonInString = new Gson().toJson(jsonObject);

                    PluginResult resultado = new PluginResult(PluginResult.Status.ERROR, jsonInString);

                    resultado.setKeepCallback(true);
                    callbackContext.sendPluginResult(resultado);
                    finish();// Exit of this activity !
                } else if (response instanceof ServiceFailure) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("errorCode", ((ServiceFailure) response).getCode());
                        jsonObject.put("errorMessage", ((ServiceFailure) response).getMessage());
                        jsonObject.put("ocrResult", ((ServiceFailure) response).getData());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String jsonInString = new Gson().toJson(jsonObject);

                    PluginResult resultado = new PluginResult(PluginResult.Status.ERROR, jsonInString);

                    resultado.setKeepCallback(true);
                    callbackContext.sendPluginResult(resultado);
                    finish();// Exit of this activity !
                } else if (response instanceof UserExit) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                         jsonObject.put("errorCode", 0);
                        jsonObject.put("step", ((UserExit) response).getStep());
                        jsonObject.put("ocrResult", ((UserExit) response).getData());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String jsonInString = new Gson().toJson(jsonObject);

                    PluginResult resultado = new PluginResult(PluginResult.Status.ERROR, jsonInString);

                    resultado.setKeepCallback(true);
                    callbackContext.sendPluginResult(resultado);

                    finish();// Exit of this activity !
                } else if (response instanceof CapturedImages) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("state", "onGoingData");
                        jsonObject.put("capturedImage", ((CapturedImages) response).getImages());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String jsonInString = new Gson().toJson(jsonObject);

                    PluginResult resultado = new PluginResult(PluginResult.Status.OK, jsonInString);

                    resultado.setKeepCallback(true);
                    callbackContext.sendPluginResult(resultado);
                }
            }
        });
    }
}