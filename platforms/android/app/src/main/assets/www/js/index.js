/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.getElementById("button").addEventListener("click",startOCR);

const baseUrl="Place_your_baseurl_here";
const token="Place_token_here" ;
const bundleKey="Place_your_bundleKey_her";
const language="en";
const documentVerification = false; // boolean, default is false
const dataValidation = false; // boolean, default is false
const returnDataValidationError = false; // boolean, default is false
const reviewData = true; // boolean, default is true
const captureOnlyMode = false; // boolean, default is false
const headers = {}; // map of key value pairs
const color="#FF018786";

const ocrParams = {
          access_token: token,
          base_url: baseUrl,
          bundle_key: bundleKey,
          language: language,
          document_verification: documentVerification,
          data_validation: dataValidation,
          return_data_validation_error: returnDataValidationError,
          review_data: reviewData,
          capture_only_mode: captureOnlyMode,
           primary_color: color
        };

function startOCR(){
        window.VIDVOCRPlugin.startOCR(
                ocrParams, headers,
                (result) => {
                  console.log('valify success result : ', result);
                  const s = result.toString();
                  const json = JSON.parse(s);
                //   alert("success");
                  // do your own logic
                },
                (error) => {
                  console.log('error in valify', error);
                  const s = error.toString();
                  const json = JSON.parse(s);
                  if (json.nameValuePairs.error_code!=0) {
                    // user exits the SDK due to an error occurred
                    alert(json.nameValuePairs.error_code);
                  } else {
                    // user exits the SDK without error
                    alert("user exits the SDK without error");
                  }
                  // do your own logic
                });

        }
